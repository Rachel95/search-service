package com.assignment.searchservice.search;

import com.assignment.searchservice.WebPageResource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController("/search")
public class SearchController {

    private final SearchService service;

    public SearchController(final SearchService service) {
        this.service = service;
    }

    @GetMapping
    public List<WebPageResource> search(@RequestParam("query") String query) {
        return service.search(query);
    }
}
