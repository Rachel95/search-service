package com.assignment.searchservice.search;

import com.assignment.searchservice.WebPageResource;
import db.model.tables.records.WebPageRecord;
import org.jooq.DSLContext;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static db.model.Tables.WEB_PAGE;

@Service
public class SearchService {

    DSLContext dslContext;

    public SearchService(DSLContext dslContext) {
        this.dslContext = dslContext;
    }

    public List<WebPageResource> search(String searchTerm) {
        return dslContext.selectFrom(WEB_PAGE)
                .where(WEB_PAGE.TITLE.like("%"+searchTerm+"%"))
                .or(WEB_PAGE.META_TAGS.like("%"+searchTerm+"%"))
                .fetch()
                .stream()
                .map(this::convertEntityToResource)
                .collect(Collectors.toList());
    }

    private WebPageResource convertEntityToResource(WebPageRecord webPageRecord) {
        WebPageResource webPageResource = new WebPageResource();
        webPageResource.setTitle(webPageRecord.getTitle());
        webPageResource.setUri(webPageRecord.getUri());
        return webPageResource;
    }
}
